# pick one of the above APIs
# do curl URL > api.txt
# use the following as a template

import json
import requests
import sys


# print(f"deleted status of the third item: { parsed_content[3]['deleted'] }")
# save the above python in a file called api.py and read in the text file you saved earlier.
# use cat to look at the text response and then pick one of the fields in your json response
# edit the python script to print that field on screen


file = open('exercise.txt') 
content = file.read()
print(f"I read the following info: {content}")
parsed_content = json.loads(content)
# print("the parsed content is: ")
# print(parsed_content)
# print(type(parsed_content))
print(parsed_content["activity"])


# Next stuff

content = requests.get('https://www.boredapi.com/api/activity') 
parsed_content = json.loads(content.text)
print(f"The parsed content is: {parsed_content}")


# Now each time we run this program, it gives a different outcome


# Now to send using APIs

SLACK_URL = "https://hooks.slack.com/services/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"

########
API_URL = '.....'


# api_response = json.loads(requests.get(API_URL).content)

########
message = "last one promise sorry"

payload = f"""
{{
 "channel": "#academy_api_testing",
 "username": "Who?",
 "text": "{message}",
 "icon_url": "https://i.kym-cdn.com/photos/images/newsfeed/001/813/481/5d7.jpg"
}}"""

r = requests.post(SLACK_URL, data=payload)

if r.status_code != 200:
    print(f"Something went wrong: status code {r.status_code}")
    exit()

print("Everything looks good")




# https://www.boredapi.com/api/activity