class LivingThing:
    pass


class Mammal(LivingThing):
    def __init__(self, name):
        self.name = name


class Human(Mammal):
    ”This is a call to create a person”
    def __init__(self, name):
        self.name = name
    def speak(self):
        print(“Hello!“)